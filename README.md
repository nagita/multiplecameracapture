# MultipleCameraCapture

## Description
- [x] 可以同時開啟最大9支的 camera 並顯示
- [x] 把影像依照間隔時間 queue 在記憶體裡面
- [x] 把記憶體裡面的影像儲存到檔案
- [ ] 依照條件自動停止把影像 queue 在記憶體裡面
- [ ] 指定輸出影像檔案名

## Requirement
1. Python 3.6+
2. Kivy == 1.11.1
3. Opencv