from kivy.app import App
from kivy.lang import Builder
from kivy.uix.boxlayout import BoxLayout
from datetime import datetime
import cv2
from kivy.uix.image import Image
from kivy.graphics.texture import Texture
from kivy.properties import StringProperty, ObjectProperty
from kivy.clock import Clock
from kivy.core.window import Window
import itertools  
import threading
Window.size = (960, 720)

Builder.load_string('''
<CameraClick>:
    orientation: 'vertical'
    GridLayout:
        cols: 3
        rows: 3
        padding: 4
        spacing: 2

        Image:
            id: camera1
            size_hint: None, None
            size: 320, 180
        Image:
            id: camera2
            size_hint: None, None
            size: 320, 180
        Image:
            id: camera3
            size_hint: None, None
            size: 320, 180

        Image:
            id: camera4
            size_hint: None, None
            size: 320, 180
        Image:
            id: camera5
            size_hint: None, None
            size: 320, 180
        Image:
            id: camera6
            size_hint: None, None
            size: 320, 180

        Image:
            id: camera7
            size_hint: None, None
            size: 320, 180
        Image:
            id: camera8
            size_hint: None, None
            size: 320, 180
        Image:
            id: camera9
            size_hint: None, None
            size: 320, 180

    # ラベル
    Label:
        id: label1
        text: root.log
        size_hint_y: None
        font_size: 20
        text_size: self.size
        halign: 'left'
		
    GridLayout:
        size_hint_y: None
        cols: 2
        rows: 2
        padding: 4
        spacing: 2
        text_size: root.size
		
        ToggleButton:
            text: 'Play'
            on_press: root.play()
            size_hint_y: None
            height: '48dp'
        ToggleButton:
            text: 'Capture'
            size_hint_y: None
            height: '48dp'
            on_press: root.capture()

        Button:
            text: 'Save'
            size_hint_y: None
            height: '48dp'
            on_press: root.save()
        Button:
            text: 'Clean Buffer'
            size_hint_y: None
            height: '48dp'
            on_press: root.clean_frame_buffer()
''')
 
 
class CameraClick(BoxLayout):
    image_texture = ObjectProperty(None)
    image_texture2 = ObjectProperty(None)	
    image_texture3 = ObjectProperty(None)
    image_texture4 = ObjectProperty(None)
    image_texture5 = ObjectProperty(None)	
    image_texture6 = ObjectProperty(None)	
    image_texture7 = ObjectProperty(None)	
    image_texture8 = ObjectProperty(None)	
    image_texture9 = ObjectProperty(None)	

    image_capture = ObjectProperty(None)
    image_capture2 = ObjectProperty(None)
    image_capture3 = ObjectProperty(None)
    image_capture4 = ObjectProperty(None)
    image_capture5 = ObjectProperty(None)
    image_capture6 = ObjectProperty(None)
    image_capture7 = ObjectProperty(None)
    image_capture8 = ObjectProperty(None)
    image_capture9 = ObjectProperty(None)
    
    log = StringProperty()

    image_frame = None
    image_frame2 = None
    image_frame3 = None
    image_frame4 = None
    image_frame5 = None
    image_frame6 = None
    image_frame7 = None
    image_frame8 = None
    image_frame9 = None	
	
    frame_buffer = []
    frame_buffer2 = []
    frame_buffer3 = []
    frame_buffer4 = []
    frame_buffer5 = []
    frame_buffer6 = []
    frame_buffer7 = []
    frame_buffer8 = []
    frame_buffer9 = []
	
    def play(self):
        global flgPlay
        flgPlay = not flgPlay
        if flgPlay == True:
            self.image_capture = cv2.VideoCapture(0)
            if self.image_capture.isOpened() == True:
                self.image_capture.set(cv2.CAP_PROP_FRAME_WIDTH, 1920)
                self.image_capture.set(cv2.CAP_PROP_FRAME_HEIGHT, 1080)
                print('Resolution: ({}, {})'.format(self.image_capture.get(cv2.CAP_PROP_FRAME_WIDTH), self.image_capture.get(cv2.CAP_PROP_FRAME_HEIGHT)))

            self.image_capture2 = cv2.VideoCapture(1)
            if self.image_capture2.isOpened() == True:
                self.image_capture2.set(cv2.CAP_PROP_FRAME_WIDTH, 1920)
                self.image_capture2.set(cv2.CAP_PROP_FRAME_HEIGHT, 1080)
                print('Resolution: ({}, {})'.format(self.image_capture2.get(cv2.CAP_PROP_FRAME_WIDTH), self.image_capture2.get(cv2.CAP_PROP_FRAME_HEIGHT)))

            self.image_capture3 = cv2.VideoCapture(2)
            if self.image_capture3.isOpened() == True:
                self.image_capture3.set(cv2.CAP_PROP_FRAME_WIDTH, 1920)
                self.image_capture3.set(cv2.CAP_PROP_FRAME_HEIGHT, 1080)
                print('Resolution: ({}, {})'.format(self.image_capture3.get(cv2.CAP_PROP_FRAME_WIDTH), self.image_capture3.get(cv2.CAP_PROP_FRAME_HEIGHT)))

            self.image_capture4 = cv2.VideoCapture(3)
            if self.image_capture4.isOpened() == True:
                self.image_capture4.set(cv2.CAP_PROP_FRAME_WIDTH, 1920)
                self.image_capture4.set(cv2.CAP_PROP_FRAME_HEIGHT, 1080)
                print('Resolution: ({}, {})'.format(self.image_capture4.get(cv2.CAP_PROP_FRAME_WIDTH), self.image_capture4.get(cv2.CAP_PROP_FRAME_HEIGHT)))

            self.image_capture5 = cv2.VideoCapture(4)
            if self.image_capture5.isOpened() == True:
                self.image_capture5.set(cv2.CAP_PROP_FRAME_WIDTH, 1920)
                self.image_capture5.set(cv2.CAP_PROP_FRAME_HEIGHT, 1080)
                print('Resolution: ({}, {})'.format(self.image_capture5.get(cv2.CAP_PROP_FRAME_WIDTH), self.image_capture5.get(cv2.CAP_PROP_FRAME_HEIGHT)))

            self.image_capture6 = cv2.VideoCapture(5)
            if self.image_capture6.isOpened() == True:
                self.image_capture6.set(cv2.CAP_PROP_FRAME_WIDTH, 1920)
                self.image_capture6.set(cv2.CAP_PROP_FRAME_HEIGHT, 1080)
                print('Resolution: ({}, {})'.format(self.image_capture6.get(cv2.CAP_PROP_FRAME_WIDTH), self.image_capture6.get(cv2.CAP_PROP_FRAME_HEIGHT)))

            self.image_capture7 = cv2.VideoCapture(6)
            if self.image_capture7.isOpened() == True:
                self.image_capture7.set(cv2.CAP_PROP_FRAME_WIDTH, 1920)
                self.image_capture7.set(cv2.CAP_PROP_FRAME_HEIGHT, 1080)
                print('Resolution: ({}, {})'.format(self.image_capture7.get(cv2.CAP_PROP_FRAME_WIDTH), self.image_capture7.get(cv2.CAP_PROP_FRAME_HEIGHT)))

            self.image_capture8 = cv2.VideoCapture(7)
            if self.image_capture8.isOpened() == True:
                self.image_capture8.set(cv2.CAP_PROP_FRAME_WIDTH, 1920)
                self.image_capture8.set(cv2.CAP_PROP_FRAME_HEIGHT, 1080)
                print('Resolution: ({}, {})'.format(self.image_capture8.get(cv2.CAP_PROP_FRAME_WIDTH), self.image_capture8.get(cv2.CAP_PROP_FRAME_HEIGHT)))

            self.image_capture9 = cv2.VideoCapture(8)
            if self.image_capture9.isOpened() == True:
                self.image_capture9.set(cv2.CAP_PROP_FRAME_WIDTH, 1920)
                self.image_capture9.set(cv2.CAP_PROP_FRAME_HEIGHT, 1080)
                print('Resolution: ({}, {})'.format(self.image_capture9.get(cv2.CAP_PROP_FRAME_WIDTH), self.image_capture9.get(cv2.CAP_PROP_FRAME_HEIGHT)))

            Clock.schedule_interval(self.update, 1.0 / 30)
        else:
            Clock.unschedule(self.update)
            self.image_capture.release()
            self.image_capture2.release()
            self.image_capture3.release()
            self.image_capture4.release()
            self.image_capture5.release()
            self.image_capture6.release()
            self.image_capture7.release()
            self.image_capture8.release()
            self.image_capture9.release()
 
    def update(self, dt):
            ret = 0
            ret2 = 0
            ret3 = 0			
            ret4 = 0			
            ret5 = 0			
            ret6 = 0			
            ret7 = 0			
            ret8 = 0			
            ret9 = 0			
            if self.image_capture.isOpened() == True:
                ret, frame = self.image_capture.read()
            if self.image_capture2.isOpened() == True:
                ret2, frame2 = self.image_capture2.read()
            if self.image_capture3.isOpened() == True:
                ret3, frame3 = self.image_capture3.read()
            if self.image_capture4.isOpened() == True:
                ret4, frame4 = self.image_capture4.read()
            if self.image_capture5.isOpened() == True:
                ret5, frame5 = self.image_capture5.read()
            if self.image_capture6.isOpened() == True:
                ret6, frame6 = self.image_capture6.read()
            if self.image_capture7.isOpened() == True:
                ret7, frame7 = self.image_capture7.read()
            if self.image_capture8.isOpened() == True:
                ret8, frame8 = self.image_capture8.read()
            if self.image_capture9.isOpened() == True:
                ret9, frame9 = self.image_capture9.read()

            if ret:
                # カメラ映像を上下左右反転
                buf = cv2.flip(frame, -1)
                image_texture = Texture.create(size=(frame.shape[1], frame.shape[0]), colorfmt='bgr')
                image_texture.blit_buffer(buf.tostring(), colorfmt='bgr', bufferfmt='ubyte')
                camera = self.ids['camera1']
                camera.texture = image_texture
                if flgCapture == True:
                    self.image_frame = frame
                    self.frame_buffer.append(self.image_frame)
                #print("frame buffer counter:[{}]".format(len(self.frame_buffer)))
                self.log = "frame buffer counter:[{}]".format(len(self.frame_buffer))

            if ret2:
                # カメラ映像を上下左右反転
                buf2 = cv2.flip(frame2, -1)
                image_texture2 = Texture.create(size=(frame2.shape[1], frame2.shape[0]), colorfmt='bgr')
                image_texture2.blit_buffer(buf2.tostring(), colorfmt='bgr', bufferfmt='ubyte')
                camera2 = self.ids['camera2']
                camera2.texture = image_texture2
                if flgCapture == True:
                    self.image_frame2 = frame2
                    self.frame_buffer2.append(self.image_frame2)

            if ret3:
                # カメラ映像を上下左右反転
                buf3 = cv2.flip(frame3, -1)
                image_texture3 = Texture.create(size=(frame3.shape[1], frame3.shape[0]), colorfmt='bgr')
                image_texture3.blit_buffer(buf3.tostring(), colorfmt='bgr', bufferfmt='ubyte')
                camera3 = self.ids['camera3']
                camera3.texture = image_texture3
                if flgCapture == True:
                    self.image_frame3 = frame3      
                    self.frame_buffer3.append(self.image_frame3)				

            if ret4:
                # カメラ映像を上下左右反転
                buf4 = cv2.flip(frame4, -1)
                image_texture4 = Texture.create(size=(frame4.shape[1], frame4.shape[0]), colorfmt='bgr')
                image_texture4.blit_buffer(buf4.tostring(), colorfmt='bgr', bufferfmt='ubyte')
                camera4 = self.ids['camera4']
                camera4.texture = image_texture4
                if flgCapture == True:
                    self.image_frame4 = frame4
                    self.frame_buffer4.append(self.image_frame4)
				
            if ret5:
                # カメラ映像を上下左右反転
                buf5 = cv2.flip(frame5, -1)
                image_texture5 = Texture.create(size=(frame5.shape[1], frame5.shape[0]), colorfmt='bgr')
                image_texture5.blit_buffer(buf5.tostring(), colorfmt='bgr', bufferfmt='ubyte')
                camera5 = self.ids['camera5']
                camera5.texture = image_texture5
                if flgCapture == True:
                    self.image_frame5 = frame5
                    self.frame_buffer5.append(self.image_frame5)

            if ret6:
                # カメラ映像を上下左右反転
                buf6 = cv2.flip(frame6, -1)
                image_texture6 = Texture.create(size=(frame6.shape[1], frame6.shape[0]), colorfmt='bgr')
                image_texture6.blit_buffer(buf6.tostring(), colorfmt='bgr', bufferfmt='ubyte')
                camera6 = self.ids['camera6']
                camera6.texture = image_texture6
                if flgCapture == True:
                    self.image_frame6 = frame6
                    self.frame_buffer6.append(self.image_frame6)

            if ret7:
                # カメラ映像を上下左右反転
                buf7 = cv2.flip(frame7, -1)
                image_texture7 = Texture.create(size=(frame7.shape[1], frame7.shape[0]), colorfmt='bgr')
                image_texture7.blit_buffer(buf7.tostring(), colorfmt='bgr', bufferfmt='ubyte')
                camera7 = self.ids['camera7']
                camera7.texture = image_texture7
                if flgCapture == True:
                    self.image_frame7 = frame7
                    self.frame_buffer7.append(self.image_frame7)

            if ret8:
                # カメラ映像を上下左右反転
                buf8 = cv2.flip(frame8, -1)
                image_texture8 = Texture.create(size=(frame8.shape[1], frame8.shape[0]), colorfmt='bgr')
                image_texture8.blit_buffer(buf8.tostring(), colorfmt='bgr', bufferfmt='ubyte')
                camera8 = self.ids['camera8']
                camera8.texture = image_texture8
                if flgCapture == True:
                    self.image_frame8 = frame8
                    self.frame_buffer8.append(self.image_frame8)

            if ret9:
                # カメラ映像を上下左右反転
                buf9 = cv2.flip(frame9, -1)
                image_texture9 = Texture.create(size=(frame9.shape[1], frame9.shape[0]), colorfmt='bgr')
                image_texture9.blit_buffer(buf9.tostring(), colorfmt='bgr', bufferfmt='ubyte')
                camera9 = self.ids['camera9']
                camera9.texture = image_texture9
                if flgCapture == True:
                    self.image_frame9 = frame9
                    self.frame_buffer9.append(self.image_frame9)
 
    def capture(self):
        '''
        '''
        global flgCapture
        flgCapture = not flgCapture

    def save(self):
        handle_thread = threading.Thread(target=self.save_thread, daemon=True)
        handle_thread.start()
 
    def save_thread(self):
        '''
        Dump all frame buffer to file
        '''		
        print("Dump image ...")
        print("frame buffer 1 size:[{}]".format(len(self.frame_buffer)))
        print("frame buffer 2 size:[{}]".format(len(self.frame_buffer2)))
        print("frame buffer 3 size:[{}]".format(len(self.frame_buffer3)))
        print("frame buffer 4 size:[{}]".format(len(self.frame_buffer4)))
        print("frame buffer 5 size:[{}]".format(len(self.frame_buffer5)))
        print("frame buffer 6 size:[{}]".format(len(self.frame_buffer6)))
        print("frame buffer 7 size:[{}]".format(len(self.frame_buffer7)))
        print("frame buffer 8 size:[{}]".format(len(self.frame_buffer8)))
        print("frame buffer 9 size:[{}]".format(len(self.frame_buffer9)))
        for i, (f1, f2, f3, f4, f5, f6, f7, f8, f9) in enumerate(itertools.zip_longest(self.frame_buffer, self.frame_buffer2, self.frame_buffer3, self.frame_buffer4, self.frame_buffer5, self.frame_buffer6, self.frame_buffer7, self.frame_buffer8, self.frame_buffer9)):
            timestr = datetime.now().strftime("%Y%m%d%H%M%S%f")
            if f1 is not None:
                cv2.imwrite("IMG1_{}.png".format(timestr), f1)
                #time.sleep(0.1)
            if f2 is not None:
                cv2.imwrite("IMG2_{}.png".format(timestr), f2)
                #time.sleep(0.1)
            if f3 is not None:
                cv2.imwrite("IMG3_{}.png".format(timestr), f3)
                #time.sleep(0.1)
            if f4 is not None:
                cv2.imwrite("IMG4_{}.png".format(timestr), f4)
                #time.sleep(0.1)
            if f5 is not None:
                cv2.imwrite("IMG5_{}.png".format(timestr), f5)
                #time.sleep(0.1)
            if f6 is not None:
                cv2.imwrite("IMG6_{}.png".format(timestr), f6)
                #time.sleep(0.1)
            if f7 is not None:
                cv2.imwrite("IMG7_{}.png".format(timestr), f7)
                #time.sleep(0.1)
            if f8 is not None:
                cv2.imwrite("IMG8_{}.png".format(timestr), f8)
                #time.sleep(0.1)
            if f9 is not None:
                cv2.imwrite("IMG9_{}.png".format(timestr), f9)
                #time.sleep(0.1)
            self.log = "save frame {}".format(i)
            #time.sleep(1)
        print("Dump image done")
        self.log = "Save image DONE!!"
        self.clean_frame_buffer()

    def clean_frame_buffer(self):
        self.frame_buffer[:] = []
        self.frame_buffer2[:] = []
        self.frame_buffer3[:] = []
        self.frame_buffer4[:] = []
        self.frame_buffer5[:] = []
        self.frame_buffer6[:] = []
        self.frame_buffer7[:] = []
        self.frame_buffer8[:] = []
        self.frame_buffer9[:] = []
        self.log = "Set frame buffer to None!!"
        	 
 
class TestCamera(App):
 
    def build(self):
        return CameraClick()
 
 
flgPlay = False
flgCapture = False
TestCamera().run()